package main

import (
 "fmt"
 "net/http"
)

func main() {

 host := "localhost"
 port := "8000"

 http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

  w.Header().Set("Content-Type", "text/html")

  fmt.Fprint(w, "Hello, World!")
 })

 fmt.Printf("Starting server on %s:%s...\n", host, port)
 http.ListenAndServe(fmt.Sprintf("%s:%s", host, port), nil)
}